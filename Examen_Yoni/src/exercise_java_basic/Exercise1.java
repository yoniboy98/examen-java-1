
	package exercise_java_basic;

	import java.util.concurrent.ThreadLocalRandom;

	public class Exercise1 {

	       public static void main(String[] args) {
	             int[] randomNumbers = generateArrayOfRandomNumbers();
	             print(randomNumbers);
	             System.out.println("");
	             System.out.println("Average : " + getAverage(randomNumbers));
	             System.out.println("Min : " + getMin(randomNumbers));
	             System.out.println("Max : " + getMax(randomNumbers));
	       }
	       
	       public static int[] generateArrayOfRandomNumbers() {
	             int[] array = new int[16];
	             for(int i = 0; i < 16; i++) {
	                    array[i] = randomInteger();
	             }
	             return array;
	       }

	       public static int randomInteger() {
	             return ThreadLocalRandom.current().nextInt(0, 100);
	       }
	       
	       public static int getMin(int[]anArray) {
	             int min = 1500;
	             for(int num : anArray) {
	                    if(num < min) {
	                          min = num;
	                    }
	             }
	             return min;
	       }
	       
	       public static int getMax(int[]anArray) {
	             int max = -1;
	             for(int num : anArray) {
	                    if(num > max) {
	                          max = num;
	                    }
	             }
	             return max;
	       }
	       
	       public static int getAverage(int []anArray) {
	             int l = anArray.length;
	             int total = 0;
	             for(int num : anArray) {
	                    total += num;
	             }
	             
	             return total / l;
	       }
	       
	       public static void print(int[]anArray) {
	             for(int i = 0; i < anArray.length; i++) {    
	                    if(i == 0){
	                          System.out.print("[");
	                    }
	                    if((anArray.length / 2) == i) {
	                          System.out.print("\n\r");
	                    }
	                    if(i == 0 || (anArray.length / 2) == i) {
	                          System.out.print(anArray[i]);
	                    } else {
	                          System.out.print("\t" + anArray[i]);
	                    }
	                    if(i == anArray.length -1) {
	                          System.out.print("]");
	                    }            
	             }}
	      
	       }


